

Function ProcessNoArgv {
    param(
        [string]$FilePath
    )
    try {
        $process = Start-Process -FilePath $FilePath -NoNewWindow -Wait -PassThru
        return $process.ExitCode
    }
    catch {
        Write-Host -ForegroundColor Red "Start: $FilePath $_"
        return $1
    }
}

Function ProcessArgv {
    param(
        [string]$FilePath,
        [string[]]$ArgumentList
    )
    try {
        $newargs = ""
        for ($i = 0; $i -lt $ArgumentList.Count; $i++) {
            [string]$arg = $ArgumentList[$i]
            if ($arg.Contains(" ") -or $arg.Contains("\t")) {
                $newargs += "`"$arg`" "
            }
            else {
                $newargs += "$arg "
            }
        }
        $process = Start-Process -FilePath $FilePath -ArgumentList $newargs -NoNewWindow -Wait -PassThru
        return $process.ExitCode
    }
    catch {
        Write-Host -ForegroundColor Red "Start: $FilePath $_"
        return $1
    }
}

Function ProcessArgvDir {
    param(
        [string]$FilePath,
        [string[]]$ArgumentList,
        [string]$Workdir
    )
    try {
        $newargs = ""
        for ($i = 0; $i -lt $ArgumentList.Count; $i++) {
            [string]$arg = $ArgumentList[$i]
            if ($arg.Contains(" ") -or $arg.Contains("\t")) {
                $newargs += "`"$arg`" "
            }
            else {
                $newargs += "$arg "
            }
        }
        $process = Start-Process -FilePath $FilePath -ArgumentList $newargs -WorkingDirectory $Workdir -NoNewWindow -Wait -PassThru
        return $process.ExitCode
    }
    catch {
        Write-Host -ForegroundColor Red "Start: $FilePath $_"
        return $1
    }
}