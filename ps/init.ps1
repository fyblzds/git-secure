$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/Git"
Import-Module -Name "$PrefixDir/Modules/Process"

$dir = $null
if ($args.Count -gt 1) {
    $dir = $args[1]
    if ($dir[0] -eq '-') {
        Write-Host -ForegroundColor Red "Not support arg: $dir"
        exit 1
    }
    $Path = $dir
}
else {
    $Path = $PWD
}


if (Test-Path "$Path/.git") {
    Write-Host "We don't support reinitialize a repository !"
    exit 1
}


$res = New-Item -ItemType Directory -Force -Path "$Path/.git/secure"
Write-Host "Initialize secure directory: $($res.FullName)"

$SecureDir = "$Path/.git/secure"

$result = ProcessArgvDir -FilePath git -ArgumentList "init" -Workdir "$Path"
if ($result -ne 0) {
    exit $result
}

$result = ProcessArgvDir -FilePath git -ArgumentList "init" -Workdir "$SecureDir"


if ($result -ne 0) {
    if ($null -eq $dir) {
        Remove-Item -Force -Recurse "$Path/*"
    }
    else {
        Remove-Item -Force -Recurse "$Path"
    }
    exit $process.ExitCode
}

Write-Host "Initialize a secure repository done"

