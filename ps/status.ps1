# show log
$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/Process"

exit (ProcessArgv -FilePath git -ArgumentList $args)