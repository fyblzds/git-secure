param(
    [string]$JsonFile
)

$Obj=Get-Content -Path $JsonFile| ConvertFrom-Json

foreach($_ in $Obj){
    Write-Host "$($_.Relative)"
}