
param(
    [string]$Commit
)

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/Git"

Get-Changeset -Commit $Commit|ForEach-Object {
    $f = $_
    switch ($f["status"]) {
        "M" {
            Write-Host -NoNewline "modify`t"
        }
        "A" {
            Write-Host -NoNewline "add`t"
        }
        "D" {
            Write-Host -NoNewline "delete`t"
        }
        Default {
            Write-Error "not support status"
        }
    }
    Write-Host $f["path"]
}
