#!/usr/bin/env powershell

param(
    [string]$Path,
    [string]$Destination
)
### convert a repository to AES encrypted repository
Import-Module -Name "$PrefixDir/Modules/AesProvider"
Import-Module -Name "$PrefixDir/Modules/Git"

Write-Host -ForegroundColor Yellow "convert a repository to AES encrypted repository"
Write-Host "Convert a repository to secure repository`nYour should have a AES 256 key, you can run git-secure key create it"
$aeskey = Read-Host "Please input your AES key"

Write-Debug "AES key: $aeskey"

