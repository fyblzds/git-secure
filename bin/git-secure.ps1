#!/usr/bin/env powershell

Function PrintUsage {
    Write-Host "Git Secure utilies 1.0
Usage: git-secure cmd args
       add         add file contents to the index
       clone       clone a encrypted repository
       config      config your secure repository
       commit      create a commit
       diff        show commit changes between commit worktree
       init        initialize a secure repository
       key         create a aes key
       pull        Fetch from and integrate with another repository or a local branch
       push        Update remote refs along with associated objects
       remote      set remote for secure repositroy
       status      Show the working tree status
       help        print help message
"
}

if ($Host.Version.Major -lt 5) {
    Write-Host -ForegroundColor Red "Git-Secure Require Powershell 5.0 or Later
Your can download WMF 5.0: https://www.microsoft.com/en-us/download/details.aspx?id=50395
Press any key to exit ..."
    $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    exit 1
}

$PrefixDir = Split-Path -Parent $PSScriptRoot
Function Help {
    param(
        [string]$cmd = "default"
    )
    if (Test-Path "$PrefixDir/docs/help/$cmd.txt") {
        Get-Content  "$PrefixDir/docs/help/$cmd.txt"|Write-Host
    }
    else {
        Write-Host -ForegroundColor Red "cmd: $cmd not found info message"
    }
}

Function Get-RegistryValueEx {
    param(
        [ValidateNotNullorEmpty()]
        [String]$Path,
        [ValidateNotNullorEmpty()]
        [String]$Key
    )
    if (!(Test-Path $Path)) {
        return 
    }
    (Get-ItemProperty $Path $Key).$Key
}


Function Test-ExecuteFile {
    param(
        [Parameter(Position = 0, Mandatory = $True, HelpMessage = "Enter Execute Name")]
        [ValidateNotNullorEmpty()]
        [String]$ExeName
    )
    $myErr = @()
    Get-command -CommandType Application $ExeName -ErrorAction SilentlyContinue -ErrorVariable +myErr
    if ($myErr.count -eq 0) {
        return $True
    }
    return $False
}

Function InitializeGitEnv {
    if (!(Test-ExecuteFile "git")) {
        $gitkey = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Git_is1"
        $gitkey2 = "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Git_is1"
        if (Test-Path $gitkey) {
            $gitinstall = Get-RegistryValueEx $gitkey "InstallLocation"
            $env:Path = "${gitinstall}\bin;${env:Path}"
        }
        elseif (Test-Path $gitkey2) {
            $gitinstall = Get-RegistryValueEx $gitkey2 "InstallLocation"
            $env:Path = "${gitinstall}\bin;${env:Path}"
        }
    }
}


Function CMDList {
    Write-Host "add`tclone`tconfig`tcommit`ndiff`tinit`tkey`tpull`npush`tremote`tstatus"
}


# Function ParseArgument{
#     param(
#         [array]$Argv
#     )
#     $
# }

if ($args.Count -eq 0) {
    PrintUsage
    exit 1
}

$newargs = ""

for ($i = 0; $i -lt $args.Count; $i++) {
    [string]$arg = $args[$i]
    if ($arg.Contains(" ") -or $arg.Contains("\t")) {
        $newargs += "`"$arg`" "
    }
    else {
        $newargs += "$arg "
    }
}

if ($PSEdition -eq "Desktop" -or $IsWindows) {
    InitializeGitEnv
}

$xcmd = $args[0]

switch ($xcmd) {
    "add" { 
        "& '$PrefixDir/ps/add.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "clone" { 
        "& '$PrefixDir/ps/clone.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "config" { 
        "& '$PrefixDir/ps/config.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "commit" {
        "& '$PrefixDir/ps/commit.ps1' $newargs" |Invoke-Expression
        exit $LASTEXITCODE
    }
    "diff" {
        "& '$PrefixDir/ps/diff.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "help" { 
        if ($args.Count -lt 2) {
            PrintUsage
            exit 0
        }
        if ($args[1] -eq "-a") {
            CMDList
            exit 0
        }
        Help -cmd $args[1]
        exit 0
    }
    "log" {
        "& '$PrefixDir/ps/log.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "init" { 
        "& '$PrefixDir/ps/init.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "key" {
        "& '$PrefixDir/ps/key.ps1'"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "pull" { 
        "& '$PrefixDir/ps/pull.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "push" { 
        "& '$PrefixDir/ps/push.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "remote" {
        "& '$PrefixDir/ps/remote.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "status" {
        "& '$PrefixDir/ps/status.ps1' $newargs"|Invoke-Expression
        exit $LASTEXITCODE
    }
    "--help" {
        PrintUsage
        exit 0
    }"-h" {
        PrintUsage
        exit 0
    }
    "--version" {
        Write-Host "git-secure 1.0"
        exit 0
    }"-v" {
        Write-Host "git-secure 1.0"
        exit 0
    }
    Default {
        Write-Host -ForegroundColor Red "unsupported command '$xcmd' your can run git-secure help -a"
        exit 1
    }
}